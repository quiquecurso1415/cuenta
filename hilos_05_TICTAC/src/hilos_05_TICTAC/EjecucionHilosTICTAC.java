/*
 * Crea dos clases q extienden de Thread. Uno d elos hilos escribir� "TIC" y el otro "TAC" en un bucle infinito.
 * Dentro del bucle ussa el m�todo sleep() para que de tiempo a ver lo que escriben.
 * El programa principal ejecutar� los dos hilos.
 * �Se visualizan los textos TIC TAC de forma ordenada?
 */

package hilos_05_TICTAC;

public class EjecucionHilosTICTAC {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		HiloTIC tic=new HiloTIC();
		HiloTAC tac=new HiloTAC();
		tic.start();
		tac.start();
	}

}
